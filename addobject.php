<?php

require 'config.php';

$update = false;

if (isset($_POST['add'])){
    $name=$_POST['name'];
    $adress=$_POST['adress'];
    $description=$_POST['description'];
    $price=$_POST['price'];
    $actual=$_POST['actual'];
    $photo=$_FILES['file']['name'];
    $target_dir = "images/";
    $upload = $target_dir . basename($_FILES["file"]["name"]);


    $imageFileType = strtolower(pathinfo($upload,PATHINFO_EXTENSION));
  // Валидация файла
  $extensions_arr = array("jpg","jpeg","png","gif");
  // Проверка формата файла
  if( in_array($imageFileType,$extensions_arr) ){
     // Загрузка файла в папку проекта
     if(move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$photo)){

    // Вставка записи
    //Проверка актуальности объявления
    $field = false;
    switch($_POST['actual'])
    {
        case 1:
            $field = true;
        break;
        
    }
    if($field){
     $query="INSERT INTO catalog(Name, Adress, Description, Price, Photo,IsActual) VALUES(?,?,?,?,?,1)";
    }
    else{
     $query="INSERT INTO catalog(Name, Adress, Description, Price, Photo,IsActual) VALUES(?,?,?,?,?,0)";
    }
    
     $stmt=$pdo->prepare($query);
     $stmt->execute(array($_POST["name"], $_POST["adress"], $_POST["description"], $_POST["price"], $_FILES['file']['name']));
     $pdo->lastInsertId('catalog');
     header('Location: ../addpage.php');
    
     }
  }
  else{ 
    header('Location: ../addpage.php');
   } 
}
if(isset($_GET['delete'])){

    $id=$_GET['delete'];

    $query = "DELETE FROM catalog WHERE id=:id";
    $stmt=$pdo->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    header('Location: ../addpage.php');
}
if(isset($_GET['edit'])){
    $id=$_GET['edit'];
    $query = "SELECT * FROM catalog WHERE id=:id";
    $stmt=$pdo->prepare($query);
    $stmt->bindParam(":id", $id);
    $stmt->execute();
    foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $row) {
    $name=$row['Name'];
    $adress=$row['Adress'];
    $description=$row['Description'];
    $price=$row['Price'];
    $actual=$row['Actual'];
    $photo=$row['Photo'];
    $actual=$row['IsActual'];
    }
    $update = true;
    
}
if (isset($_POST['upbtn'])){
    $id=$_POST['id'];
    $name=$_POST['name'];
    $adress=$_POST['adress'];
    $description=$_POST['description'];
    $price=$_POST['price'];
    $actual=$_POST['actual'];
    $oldphoto =$_POST['old_file']; 
    $target_dir = "images/";
    $upload = $target_dir . basename($_FILES["file"]["name"]);
   
    if(isset($_FILES["old_file"]["name"]) && ($_FILES["file"]["name"]!="")){
        $newphoto="images/".$_FILES["file"]["name"] ;
        unlink($oldphoto);
        move_uploaded_file($_FILES['file']['tmp_name'], $newphoto);

    }
else 
{
    $newphoto=$_FILES["file"]["name"];
}    
    $field = false;
    switch($_POST['actual'])
    {
        case 1:
            $field = true;
        break;
        
    }
    if($field){
        $query="UPDATE catalog SET Name=?, Adress=?, Description=?, Price=?, Photo=?,IsActual=1 WHERE Id=?";
    }
    else{
        $query="UPDATE catalog SET Name=?, Adress=?, Description=?, Price=?, Photo=?,IsActual=0 WHERE Id=?";
    }
    
    $stmt=$pdo->prepare($query);
    $stmt->execute(array($_POST["name"], $_POST["adress"], $_POST["description"], $_POST["price"], $newphoto, $_POST['id']));
    
    header('Location: ../addpage.php');
}

?>