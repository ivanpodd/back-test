<?php
session_start();
require_once 'config.php';

$login = trim($_POST['login']);
$password = trim($_POST['password']);

$sql = "SELECT * FROM `users` WHERE `login` = ?";
$query = $pdo->prepare($sql);
$query->execute([$login]);
$user = $query->fetch(PDO::FETCH_ASSOC);
if($user && $password) {

    $_SESSION['user'] = [
        "role" => $user['role']
    ];
    header('Location: ../index.php');
} 
else {
    echo '<script>alert("Такой пользователь не найден")</script>';
    exit();
}

?>