<?php
session_start();
require 'config.php'; 
 ?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <title>Поиск недвижимости</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Latest compiled and minified CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    
</head>

<body>
    <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#">Logo</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
            <div class="collapse navbar-collapse" id="mynavbar">
                <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Главная</a>
                        </li>
                <?php if (isset($_SESSION['user'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="addpage.php">Добавить объект</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Выйти</a>
                        </li>
                <?php } else { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Войти</a>
                        </li>
                <?php  } ?>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-10 bg-light mt-2 rounded pb-3">
                <h1 class="text-primary p-2">Все объявления</h1>
                <hr>
                <div class="form-inline">
                    <label for="search" class="font-weight-bold lead text-dark">Введите номер дома</label> &nbsp; &nbsp; &nbsp;
                    <input type="text" name="search" class="form-control form-control-lg rounded-0 border-primary" id="search">
                </div>
                <hr>
               
                 <?php  $data = $pdo->query('SELECT * FROM catalog'); ?>
                <table class="table table-hover table-light table-striped" id="table-data">
                    <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Наименование объекта</th>
                            <th>Адрес</th>
                            <th>Описание</th>
                            <th>Цена</th>
                        </tr>
                    </thead>
                    <tbody id="output">
                    <?php  foreach ($data as $row){
                        if($row['IsActual']== 1){  ?>
                        <tr>
                            <td><img class="img-thumbnail" width="300" src="/images/<?php echo $row['Photo']; ?>" alt="изображение недвижимости"></td>
                            <td><?php echo $row['Name']; ?></td>
                            <td><?php echo $row['Adress']; ?></td>
                            <td><?php echo $row['Description']; ?></td>
                            <td><?php echo $row['Price']; ?></td>
                        </tr>
                        <?php }  } ?>
                    </tbody>
                </table>
            </div>
          
        </div>
    </div>
    <script type="text/javascript">
  $(document).ready(function(){
    $("#search").keyup(function(){
      $.ajax({
        type:'POST',
        url:'search.php',
        data:{
          name:$("#search").val(),
        },
        success:function(data){
          $("#output").html(data);
        }
      });
    });
  });
</script>
</body>
</html>