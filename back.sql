-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 21 2022 г., 02:26
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `back`
--

-- --------------------------------------------------------

--
-- Структура таблицы `catalog`
--

CREATE TABLE `catalog` (
  `id` int(11) NOT NULL,
  `Name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Adress` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Price` int(11) NOT NULL,
  `Photo` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `IsActual` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `catalog`
--

INSERT INTO `catalog` (`id`, `Name`, `Adress`, `Description`, `Price`, `Photo`, `IsActual`) VALUES
(1, 'Объект 1', 'ул. Мамадышская 45-78', '10-ый дом, 180 кв. метров, двухуровневая', 1000000, '16.jpg', 1),
(2, 'Объект2', 'ул. Гагарина 56-56', 'ул. Азата Аббасова, д. 11, Казань', 4900000, '1930134.jpg', 1),
(4, 'Объект 3', 'ул. Ленинградская 97-01', 'частный дом из сруба, 100 кв.метров', 4500000, 'dom-niva-178145527-2.jpg', 1),
(5, 'Объект 4', 'ул. Галимджана Баруди 59-78', 'Кирпичный дом, 80 кв. метров', 3200000, 'dom-gorodishce-141072644-2.jpg', 1),
(6, 'Объект 5', 'ул. 50 лет Победы 24-32', 'Панельный дом, 45 кв. метров, с двумя лоджиями', 2500000, 'type_b.jpg', 0),
(7, 'Объект 6', 'ул. Аббасова 11-22', 'Продается 2-комн. кв., 64 м2, 9/19 этаж', 3900000, 'R3.jpg', 0),
(8, 'Объект 7', ' ул. Лушникова 50-7', 'Продается 2-комн. кв., 45.8 м2, 1/5 этаж', 2600000, '1944656.jpg', 1),
(9, 'Объект 8', 'ул. Широка 97-01', 'Продается 2-комн. кв., 63 м2, 9/10 этаж', 4350000, 'foto_largest.jpg', 0),
(10, 'Объект 9', 'ул. Хо Ши Мина  56-321', 'Продается 4-комн. кв., 83.5 м2, 10/11 этаж', 3750000, 'самая-дорогая-квартира-в-Москве.jpg', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `Login` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`Id`, `Login`, `Password`, `Role`) VALUES
(1, 'admin', 'admin', 1),
(1, 'admin', 'admin', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `catalog`
--
ALTER TABLE `catalog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
