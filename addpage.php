<?php
session_start();
require 'config.php';
require 'addobject.php'; 
 ?>
    <!DOCTYPE html>
    <html lang="ru">

    <head>
        <title>Поиск недвижимости</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Latest compiled and minified CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Latest compiled JavaScript -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    </head>

    <body>
        <nav class="navbar navbar-expand-sm navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Logo</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mynavbar">
            <span class="navbar-toggler-icon"></span>
          </button>
                <div class="collapse navbar-collapse" id="mynavbar">
                    <ul class="navbar-nav me-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Главная</a>
                        </li>
                        <?php if (isset($_SESSION['user'])) { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Добавить объект</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="logout.php">Выйти</a>
                        </li>
                        <?php } else { ?>
                        <li class="nav-item">
                            <a class="nav-link" href="signin.php">Войти</a>
                        </li>
                        <?php  } ?>
                    </ul>

                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <h3 class="text-center text-dark mt-2">Панель управления объявлениями</h3>
                    <hr>
                   
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h3 class="text-center text-info">Добавить объект</h3>
                 
                    <form action="addobject.php" method="post" enctype="multipart/form-data" id="form_id" onsubmit="javascript:return validate('form_id','adress');">
                        <input type="hidden" name="id" value="<?= $id; ?>">
                        <div class="form-group mt-2">
                            <input type="text" name="name" value="<?= $name; ?>" class="form-control" placeholder="Имя объекта" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" name="adress" value="<?= $adress ?>" class="form-control" placeholder="Адрес объекта" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="text" name="description" value="<?= $description ?>" class="form-control" placeholder="Описание объекта" required>
                        </div>
                        <div class="form-group mt-2">
                            <input type="number" name="price" value="<?= $price ?>" class="form-control" placeholder="Цена объекта" required>
                        </div>
                        <div class="form-group mt-2">
                        <input type="hidden" name="old_file" value="<?= $photo; ?>">
                            <input type="file" name="file" class="custom-file" >
                            <img src="images/<?= $photo ?>" class="img-thumbnail" width="120" alt="">
                        </div>
                        <div class="form-check mt-2">
                        <?php if($actual == 1) { ?> 
  <input class="form-check-input" name="actual" type="checkbox" value="1" id="flexCheckChecked" checked>
  <?php } else { ?>
    <input class="form-check-input" name="actual" type="checkbox" value="1" id="flexCheckChecked">
    <?php }  ?>
  <label class="form-check-label" for="flexCheckChecked">
    Актуально
  </label>
</div>
                        <div class="form-group mt-2">
                            <?php if($update == true) { ?>
                                <input type="submit" name="upbtn" class="btn btn-success btn-block" value="Обновить"></input>
                                <?php } else { ?>
                        <input type="submit" name="add" class="btn btn-primary btn-block" value="Сохранить"></input>
                        <?php }  ?>
                        </div>
                    </form>
                </div>
                
                <div class="col-md-8">
                <h3 class="text-center text-info">Все объявления</h3>
                <?php  $data = $pdo->query('SELECT * FROM catalog'); ?>
                <table class="table table-hover table-light table-striped" id="table-data">
                    <thead>
                        <tr>
                            <th>Фото</th>
                            <th>Наименование объекта</th>
                            <th>Адрес</th>
                            <th>Описание</th>
                            <th>Цена</th>
                            <th>Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php  foreach ($data as $row){  ?>
                        <tr>
                            <td><img class="img-thumbnail" width="150" src="/images/<?php echo $row['Photo']; ?>" alt="изображение недвижимости"></td>
                            <td><?php echo $row['Name']; ?></td>
                            <td><?php echo $row['Adress']; ?></td>
                            <td><?php echo $row['Description']; ?></td>
                            <td><?php echo $row['Price']; ?></td>
                            <td>
                                <a href="addpage.php?edit=<?= $row['id']; ?>" class="badge  bg-success p-2">Редактировать</a>
                                <a href="addobject.php?delete=<?= $row['id']; ?>" class="badge  bg-danger p-2" onclick="return confirm('Удалить данную запись?');">Удалить</a>
                            </td>
                        </tr>
                        <?php  } ?>
                    </tbody> 
                </table>
                </div>
            </div>
        </div>
    </body>
    <script>
  function validate(form_id,adress) { 
   var reg = /^[0-9а-я\s.]+?\d+-\s?\d/i;
   var address = document.forms[form_id].elements[adress].value;
   if(reg.test(address) == false) {
      alert('Формат удреса: ул.Название НомерДома-НомерКвартиры');
      return false;
   }
}
 </script>
    </html>